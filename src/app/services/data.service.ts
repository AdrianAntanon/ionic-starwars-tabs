import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RespuestaCharacters } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  BASE_URL: string = `https://swapi.dev/api/people/`;
  pageNumber: number = 1;
  constructor(private http: HttpClient) {}

  getCharacters() {
    return this.http.get<RespuestaCharacters>(
      `${this.BASE_URL}?page=${this.pageNumber}`
    );
  }
  getMoreInfo() {
    this.pageNumber++;
  }
}
