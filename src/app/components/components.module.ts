import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CharactersComponent } from './characters/characters.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [CharactersComponent],
  imports: [CommonModule, IonicModule],
  exports: [CharactersComponent],
})
export class ComponentsModule {}
