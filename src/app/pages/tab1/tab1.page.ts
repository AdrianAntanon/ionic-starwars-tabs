import { Component, OnInit } from '@angular/core';
import { Character } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  characters: Character[] = [];
  constructor(private data: DataService) {}

  loadCharacters(event?) {
    this.data.getCharacters().subscribe((response) => {
      console.log('Characters', response);
      if (response.next === null) {
        event.target.disabled = true;
        event.target.complete();
        return;
      }
      this.characters.push(...response.results);
      console.log(response.next);
      if (event) {
        event.target.complete();
      }
    });
  }

  loadData(event) {
    this.data.getMoreInfo();
    this.loadCharacters(event);
  }

  ngOnInit(): void {
    this.loadCharacters();
  }
}
